﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Vector3 itemPos;

            cubePlayer = new Cube()
            {
                pos = new Vector3(0, 0, 1),
                mass = 3.0f,
                col = new Models.Color(1, 1, 1, 1),
                size = 1.0f
            };

            do
            {
                itemPos = new Vector3()
                {
                    x = (float)RandomNumberGenerator.GenerateDouble(0, 35),
                    y = (float)RandomNumberGenerator.GenerateDouble(0, 35),
                    z = 1
                };
            } while (itemPos == cubePlayer.pos);

            item = new Cube()
            {
                pos = itemPos,
                col = new Models.Color(0, 0.5, 1, 1),
                size = 1.25f
            };
            
            for (int x = 0; x < 10; x++)
            {
                itemPos = new Vector3()
                {
                    x = (float)RandomNumberGenerator.GenerateDouble(-35, 35),
                    y = (float)RandomNumberGenerator.GenerateDouble(-35, -10),
                    z = 1
                };

                if (x > 0)
                {
                    while (itemPos == dangerCubes[x-1].pos)
                    {
                        itemPos = new Vector3()
                        {
                            x = (float)RandomNumberGenerator.GenerateDouble(-35, 35),
                            y = (float)RandomNumberGenerator.GenerateDouble(-35, -10),
                            z = 1
                        };
                    }
                }

                dangerCubes.Add(new Cube()
                {
                    pos = itemPos,
                    col = new Models.Color((float)RandomNumberGenerator.GenerateDouble(0.75, 1.0), (float)RandomNumberGenerator.GenerateDouble(0.1, 0.5), 0, 0.75),
                    size = (float)RandomNumberGenerator.GenerateDouble(1.0, 1.5),
                    mass = (float)RandomNumberGenerator.GenerateDouble(1.0, 2.5)
                });
            }

            if (cubePlayer.velocity.x == 0.0f)
            {
                cubePlayer.ApplyForce(wind);
            }
        }

        // declarations
        // float rotation = 0.0f;
        
        private float time = 0.0f;

        private Vector3 mousePos = new Vector3();

        private Vector3 wind = new Vector3(2.0f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.5f, 0);
        private Vector3 helium = new Vector3(0, 1.0f, 0);
        private Vector3 zero = new Vector3(0, 0, 0);

        private Cube cubePlayer;
        private Cube item;
        private List<Cube> dangerCubes = new List<Cube>();

        private bool isTouchingDangerCubes = false;
        private int score = 0;

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT D7";
            OpenGL gl = args.OpenGL;

            // clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            // draw
            //gl.Rotate(rotation, 0.0f, 0.0f, 0.0f); // rotation value, bool, bool, bool
            //Teapot tp = new Teapot();
            //tp.Draw(gl, 14, 3, OpenGL.GL_FILL);
            //rotation += 30.0f;

            time += 1.0f;
            if (time == 10.0f)
            {
                time = 1.0f;
            }

            mousePos.Normalize();

            if (cubePlayer != null)
            {
                if (Keyboard.IsKeyDown(Key.Space))
                {
                    cubePlayer.ApplyForce((((helium * 2.0f) - gravity) * 2.0f));
                    if (cubePlayer.velocity.y > 2.0f)
                    {
                        cubePlayer.velocity.y = 2.0f;
                    }
                }

                if ((cubePlayer.pos.x + cubePlayer.size >= item.pos.x - item.size && cubePlayer.pos.x - cubePlayer.size <= item.pos.x + item.size) &&
                    (cubePlayer.pos.y + cubePlayer.size >= item.pos.y - item.size && cubePlayer.pos.y - cubePlayer.size <= item.pos.y + item.size))
                {
                    cubePlayer.pos = new Vector3()
                    {
                        x = (float)RandomNumberGenerator.GenerateDouble(0, 35),
                        y = (float)RandomNumberGenerator.GenerateDouble(0, 35),
                        z = 1
                    };

                    item.pos = new Vector3()
                    {
                        x = (float)RandomNumberGenerator.GenerateDouble(0, 35),
                        y = (float)RandomNumberGenerator.GenerateDouble(0, 35),
                        z = 1
                    };

                    cubePlayer.velocity.x *= -1;

                    score++;

                    isTouchingDangerCubes = true;
                }

                for (int x = 0; x < dangerCubes.Count; x++)
                {
                    if ((cubePlayer.pos.x + cubePlayer.size >= dangerCubes[x].pos.x - dangerCubes[x].size && cubePlayer.pos.x - cubePlayer.size <= dangerCubes[x].pos.x + dangerCubes[x].size) &&
                        (cubePlayer.pos.y + cubePlayer.size >= dangerCubes[x].pos.y - dangerCubes[x].size && cubePlayer.pos.y - cubePlayer.size <= dangerCubes[x].pos.y + dangerCubes[x].size))
                    {
                        cubePlayer = null;
                        break;
                    }
                }

                if (isTouchingDangerCubes)
                {
                    for (int x = 0; x < 10; x++)
                    {
                        Vector3 itemPos = new Vector3()
                        {
                            x = (float)RandomNumberGenerator.GenerateDouble(-35, 35),
                            y = (float)RandomNumberGenerator.GenerateDouble(-35, -10),
                            z = 1
                        };

                        if (x > 0)
                        {
                            while (itemPos == dangerCubes[x - 1].pos)
                            {
                                itemPos = new Vector3()
                                {
                                    x = (float)RandomNumberGenerator.GenerateDouble(-35, 35),
                                    y = (float)RandomNumberGenerator.GenerateDouble(-35, -10),
                                    z = 1
                                };
                            }
                        }

                        dangerCubes[x].pos = itemPos;
                    }

                    isTouchingDangerCubes = false;
                }

                if (cubePlayer != null)
                {
                    if (cubePlayer.pos.x > 40.0f)
                    {
                        cubePlayer.pos.x = 40.0f;
                        cubePlayer.velocity.x *= -1;
                    }

                    if (cubePlayer.pos.x < -40.0f)
                    {
                        cubePlayer.pos.x = -40.0f;
                        cubePlayer.velocity.x *= -1;
                    }

                    if (cubePlayer.pos.y > 40.0f)
                    {
                        cubePlayer.pos.y = 40.0f;
                        cubePlayer.velocity.y *= -1;
                        cubePlayer.ApplyForce(gravity);
                    }

                    if (cubePlayer.pos.y < -40.0f)
                    {
                        cubePlayer.pos.y = -40.0f;
                        cubePlayer.velocity.y *= -1;
                        cubePlayer.ApplyForce(gravity);
                    }
                    cubePlayer.ApplyForce(gravity);
                    cubePlayer.Render(gl);
                }

                foreach (var dc in dangerCubes)
                {
                    if (dc.pos.x > 40.0f)
                    {
                        dc.pos.x = 40.0f;
                        dc.velocity.x *= -1;
                    }

                    if (dc.pos.x < -40.0f)
                    {
                        dc.pos.x = -40.0f;
                        dc.velocity.x *= -1;
                    }

                    if (dc.pos.y > 40.0f)
                    {
                        dc.pos.y = 40.0f;
                        dc.velocity.y *= -1;
                        dc.ApplyForce(gravity);
                    }

                    if (dc.pos.y < -40.0f)
                    {
                        dc.pos.y = -40.0f;
                        dc.velocity.y *= -1;
                        dc.ApplyForce(gravity);
                    }

                    if (dc.pos.y < -35.0f)
                    {
                        dc.ApplyForce(helium/2.0f);
                    }

                    dc.pos += new Vector3((float)RandomNumberGenerator.GenerateDouble(-2.5, 2.5) + (float)Math.Sin(time/2.0f) * (float)Math.Sin(Math.Floor(time)), (float)RandomNumberGenerator.GenerateDouble(-1.5, 2.5) * (float)Math.Sin(time) + (float)Math.Cos(time / 2.0f), 0);
                }
            }
            else
            {
                gl.DrawText(205, 320, 1.0f, 0.0f, 0.0f, "Arial", 40.0f, "GAME OVER");
                foreach (var dc in dangerCubes)
                {
                    if (dc.pos.x > 40.0f)
                    {
                        dc.pos.x = 40.0f;
                        dc.velocity.x *= -1;
                    }

                    if (dc.pos.x < -40.0f)
                    {
                        dc.pos.x = -40.0f;
                        dc.velocity.x *= -1;
                    }

                    if (dc.pos.y > 40.0f)
                    {
                        dc.pos.y = 40.0f;
                        dc.velocity.y *= -1;
                        dc.ApplyForce(gravity);
                    }

                    if (dc.pos.y < -40.0f)
                    {
                        dc.pos.y = -40.0f;
                        dc.velocity.y *= -1;
                        dc.ApplyForce(gravity);
                    }

                    dc.ApplyForce(gravity);
                }
            }

            gl.DrawText(100, 550, 1.0f, 0.5f, 0.5f, "Arial", 20.0f, "Score: " + score.ToString());
            item.Render(gl);

            foreach (var dc in dangerCubes)
            {
                dc.Render(gl);
            }
        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f};

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);


            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        #region MOUSE MOVEMENT
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePos.x = (float)position.X - (float)Width / 2.0f;
            mousePos.y = -((float)position.Y - (float)Height / 2.0f);
        }
        #endregion
    }
}
