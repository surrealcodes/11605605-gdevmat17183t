﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Cube : Movable
    {

        public float size;
        
        public Cube(float x = 0, float y = 0, float z = 0, float s = 1.0f)
        {
            this.pos.x = x;
            this.pos.y = y;
            this.pos.z = z;
            size = s;
            size /= 2.0f;
        }

        public Cube(Vector3 initPos)
        {
            this.pos = initPos;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(col.r, col.g, col.b, col.a);
            gl.Begin(OpenGL.GL_QUADS);
            // front
            gl.Vertex(this.pos.x - size, this.pos.y + size);
            gl.Vertex(this.pos.x + size, this.pos.y + size);
            gl.Vertex(this.pos.x + size, this.pos.y - size);
            gl.Vertex(this.pos.x - size, this.pos.y - size);
            gl.End();

            UpdateMotion();
        }

        private void UpdateMotion()
        {
            this.velocity += this.accel; // apply accel
            this.pos += this.velocity; // update motion
            this.accel *= 0;
        }
    }
}
