﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Cube
    {
        public float posX, posY;
        public Color color = new Color(1, 0, 0);

        public Cube(float x = 0, float y = 0)
        {
            this.posX = x;
            this.posY = y;
        }

        public void Render(OpenGL gl)
        {
            gl.Color(color.r, color.g, color.b);
            gl.Begin(OpenGL.GL_QUADS);
            // front
            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);
            gl.End();
        }
    }
}
