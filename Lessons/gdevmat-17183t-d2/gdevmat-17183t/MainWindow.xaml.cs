﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // declarations
        // float rotation = 0.0f;
   
        private float time = 0.0f;
        private float interval = 0.25f;
        private Cube myCube = new Cube();
        private string dir = "";
        private const int min = 0;
        private const int max = 10;
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT D2";
            OpenGL gl = args.OpenGL;

            // clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            // draw
            //gl.Rotate(rotation, 0.0f, 0.0f, 0.0f); // rotation value, bool, bool, bool
            //Teapot tp = new Teapot();
            //tp.Draw(gl, 14, 3, OpenGL.GL_FILL);
            //rotation += 30.0f;

            // time value
            time += 0.75f;
            if (time >= 360.0f) time = 0;

            myCube.Render(gl);

            double outcome = RandomNumberGenerator.GenerateDouble(0, 1);

            if (outcome <= 0.4)
            {
                // only 40% clamped
                myCube.posX += interval;
                dir = "E";
            }
            else if (outcome >= 0.4)
            {
                // all 20% even distrib
                outcome = RandomNumberGenerator.GenerateInt(0, 2);
                switch (outcome)
                {
                    case 0:
                        myCube.posY += interval;
                        dir = "N";
                        break;
                    case 1:
                        myCube.posY -= interval;
                        dir = "S";
                        break;
                    case 2:
                        myCube.posX -= interval;
                        dir = "W";
                        break;
                }
            }

            //switch (RandomNumberGenerator.GenerateInt(min, max))
            //{
            //    case 0:
            //        myCube.posY += interval;
            //        dir = "N";
            //        break;

            //    case 1:
            //        myCube.posY -= interval;
            //        dir = "S";
            //        break;
            //    case 2:
            //        myCube.posX += interval;
            //        dir = "E";
            //        break;
            //    case 3:
            //        myCube.posX -= interval;
            //        dir = "W";
            //        break;
            //    case 4:
            //        myCube.posX += interval;
            //        myCube.posY += interval;
            //        dir = "NE";
            //        break;
            //    case 5:
            //        myCube.posX -= interval;
            //        myCube.posY += interval;
            //        dir = "NW";
            //        break;
            //    case 6:
            //        myCube.posX += interval;
            //        myCube.posY -= interval;
            //        dir = "SE";
            //        break;
            //    case 7:
            //        myCube.posX -= interval;
            //        myCube.posY -= interval;
            //        dir = "SW";
            //        break;
            //    case 8:
            //        dir = "--";
            //        break;
            //}

            int rand_r = RandomNumberGenerator.GenerateInt(0, 1);
            int rand_g = RandomNumberGenerator.GenerateInt(0, 1);
            int rand_b = RandomNumberGenerator.GenerateInt(0, 1);

            myCube.color = new Models.Color(rand_r, rand_g, rand_b);
            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, dir + " at " + outcome.ToString());
        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f};

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
