﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public abstract class Movable
    {
        public Vector3 pos = new Vector3();
        public Vector3 velocity = new Vector3();
        public Vector3 accel = new Vector3();
        public float mass = 1;

        public Color col = new Color();

        public void ApplyForce(Vector3 force)
        {
            // f = ma
            // a = f/m
            this.accel += (force / mass); // force accumulation
        }

        public abstract void Render(OpenGL gl);
    }
}
