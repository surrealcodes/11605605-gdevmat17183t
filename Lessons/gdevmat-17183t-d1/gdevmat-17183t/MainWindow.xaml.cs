﻿using SharpGL;
using SharpGL.SceneGraph.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // declarations
        // float rotation = 0.0f;
        private const float graphLimit = 10.0f;
        private const float iteration = 0.1f;
        private const float angleLimit = 360.0f;
        private float time = 0.0f;
        private float freq = 0.0f;
        private float amp = 0.0f;
        private float posX = 0.0f;
        private float posY = 0.0f;

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT FIRST DAY";
            OpenGL gl = args.OpenGL;

            // clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            // draw
            //gl.Rotate(rotation, 0.0f, 0.0f, 0.0f); // rotation value, bool, bool, bool
            //Teapot tp = new Teapot();
            //tp.Draw(gl, 14, 3, OpenGL.GL_FILL);
            //rotation += 30.0f;

            // time value
            time += 0.75f;
            if (time >= angleLimit) time = 0;

            // keyboard input
            if (Keyboard.IsKeyDown(Key.Up))
            {
                amp += 0.1f;
            }

            if (Keyboard.IsKeyDown(Key.Down))
            {
                amp -= 0.1f;
            }

            if (Keyboard.IsKeyDown(Key.Left))
            {
                freq -= 0.1f;
            }

            if (Keyboard.IsKeyDown(Key.Right))
            {
                freq += 0.5f;
            }

            if (Keyboard.IsKeyDown(Key.W))
            {
                posY += 0.5f;
            }

            if (Keyboard.IsKeyDown(Key.A))
            {
                posX -= 0.5f;
            }

            if (Keyboard.IsKeyDown(Key.S))
            {
                posY -= 0.5f;
            }

            if (Keyboard.IsKeyDown(Key.D))
            {
                posX += 0.5f;
            }

            // drawing proper
            gl.PointSize(3.5f);
            gl.Begin(OpenGL.GL_POINTS);
            for (float x = -graphLimit; x < graphLimit; x += iteration)
            {
                gl.Vertex(x, Math.Sin(x*freq+time)*amp);
            }
            gl.End();

            float radius = 1.0f;
            gl.PointSize(3.5f);
            gl.Begin(OpenGL.GL_POINTS);
            for (float x = 0.0f; x < angleLimit; x += iteration)
            {
                gl.Vertex(Math.Cos(x)*radius+posX, Math.Sin(x)*radius+posY);
            }
            gl.End();
            
            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 10);
            gl.Vertex(0, -10);
            gl.End();

            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(10, 0);
            gl.Vertex(-10, 0);
            gl.End();

            for (float x = -10.0f; x < 10.0f; x += 1.0f)
            {
                gl.LineWidth(0.5f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(x, 0.25);
                gl.Vertex(x, -0.25);
                gl.End();

                gl.LineWidth(0.5f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(0.25, x);
                gl.Vertex(-0.25, x);
                gl.End();

                gl.LineWidth(2.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(Math.Cos(x)*radius*2.5f, Math.Sin(x)*radius*2.5f);
                gl.Vertex(Math.Cos(x+2.0f)*radius*2.5f, Math.Sin(x+2.0f)*radius*2.5);
                gl.End();
            }
        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f};

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
