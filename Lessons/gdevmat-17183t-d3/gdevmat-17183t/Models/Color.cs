﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Color
    {
        public double r, g, b;
        public double a = 1;

        public Color (double r = 1, double g = 1, double b = 1)
        {
            this.r = r;
            this.g = g;
            this.b = b;

        }
    }
}
