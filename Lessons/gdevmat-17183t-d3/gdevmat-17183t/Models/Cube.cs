﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Cube : Movable
    {
        
        public Cube(float x = 0, float y = 0, float r = 0, float g = 0, float b = 0, float a = 1)
        {
            this.posX = x;
            this.posY = y;
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(r, g, b, a);
            gl.Begin(OpenGL.GL_QUADS);
            // front
            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);
            gl.End();
        }
    }
}
