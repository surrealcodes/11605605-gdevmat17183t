﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Circle : Movable
    {
        private const float angleLimit = 360.0f;
        private const float smoothing = 1.0f;
        public float radius;

        public Circle(float x = 0, float y = 0, float rad = 1.0f, float r = 0, float g = 0, float b = 0, float a = 1)
        {
            this.posX = x;
            this.posY = y;
            this.radius = rad;
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public override void Render(OpenGL gl)
        {
            gl.PointSize(1.5f);
            gl.Color(r, g, b, a);
            gl.Begin(OpenGL.GL_LINES);
            for (float x = 0.0f; x < angleLimit; x += smoothing)
            {
                gl.Vertex(posX, posY);
                gl.Vertex(Math.Cos(x) * radius + posX, Math.Sin(x) * radius + posY);
            }
            gl.End();
        }
    }
}
