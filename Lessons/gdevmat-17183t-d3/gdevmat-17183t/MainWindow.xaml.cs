﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // declarations
        // float rotation = 0.0f;
   
        private float time = 0.0f;
        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT D3";
            OpenGL gl = args.OpenGL;

            // clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            // draw
            //gl.Rotate(rotation, 0.0f, 0.0f, 0.0f); // rotation value, bool, bool, bool
            //Teapot tp = new Teapot();
            //tp.Draw(gl, 14, 3, OpenGL.GL_FILL);
            //rotation += 30.0f;

            // time value
            time += 0.1f;
            if (time >= 300.0f)
            {
                cubes.Clear();
                circles.Clear();
                gl.ClearColor(0, 0, 0, 1);
                gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
                gl.LoadIdentity();
                time = 0;
            }

            circles.Add(new Circle()
            {
                r = 0.25,
                g = 0.15,
                b = 0.5,
                a = RandomNumberGenerator.GenerateDouble(0, 1),
                posX = (float)RandomNumberGenerator.GenerateGaussian(0, 15),
                posY = (float)RandomNumberGenerator.GenerateGaussian(0, 15),
                radius = (float)RandomNumberGenerator.GenerateGaussian(0, 15)
            });

            cubes.Add(new Cube()
            {
                r = RandomNumberGenerator.GenerateDouble(0, 1),
                g = RandomNumberGenerator.GenerateDouble(0, 1),
                b = RandomNumberGenerator.GenerateDouble(0, 1),
                a = RandomNumberGenerator.GenerateDouble(0, 1),
                posX = RandomNumberGenerator.GenerateInt(-50, 50),
                posY = RandomNumberGenerator.GenerateInt(-50, 50)
            });

            foreach (var cb in cubes)
            {
                cb.Render(gl);
            }

            foreach (var c in circles)
            {
                c.Render(gl);
            }

        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f};

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);


            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
