﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Cube : Movable
    {
        
        public Cube(float x = 0, float y = 0, float z = 0)
        {
            this.pos.x = x;
            this.pos.y = y;
            this.pos.z = z;
        }

        public Cube(Vector3 initPos)
        {
            this.pos = initPos;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(col.r, col.g, col.b, col.a);
            gl.Begin(OpenGL.GL_QUADS);
            // front
            gl.Vertex(this.pos.x - (this.mass/2.0f), this.pos.y + (this.mass / 2.0f));
            gl.Vertex(this.pos.x + (this.mass / 2.0f), this.pos.y + (this.mass / 2.0f));
            gl.Vertex(this.pos.x + (this.mass / 2.0f), this.pos.y - (this.mass / 2.0f));
            gl.Vertex(this.pos.x - (this.mass / 2.0f), this.pos.y - (this.mass / 2.0f));
            gl.End();

            UpdateMotion();
        }

        private void UpdateMotion()
        {
            this.velocity += this.accel; // apply accel
            this.pos += this.velocity; // update motion
            this.accel *= 0;
        }
    }
}
