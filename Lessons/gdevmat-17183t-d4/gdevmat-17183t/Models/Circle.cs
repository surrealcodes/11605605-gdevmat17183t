﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Circle : Movable
    {
        private const float angleLimit = 360.0f;
        private const float smoothing = 1.0f;
        public float radius;

        public Circle(float x = 0, float y = 0, float z = 0, float rad = 1.0f)
        {
            this.pos.x = x;
            this.pos.y = y;
            this.pos.z = z;
            this.radius = rad;
        }

        public Circle(Vector3 initPos, float rad = 1.0f)
        {
            this.pos = initPos;
            radius = rad;
        }

        public override void Render(OpenGL gl)
        {
            gl.PointSize(1.5f);
            gl.Color(col.r, col.g, col.b, col.a);
            gl.Begin(OpenGL.GL_LINES);
            for (float x = 0.0f; x < angleLimit; x += smoothing)
            {
                gl.Vertex(pos.x, pos.y);
                gl.Vertex(Math.Cos(x) * radius + pos.x, Math.Sin(x) * radius + pos.y);
            }
            gl.End();
        }
    }
}
