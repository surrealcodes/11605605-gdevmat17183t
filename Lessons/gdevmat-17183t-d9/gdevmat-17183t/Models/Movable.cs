﻿using gdevmat_17183t.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public abstract class Movable
    {
        public Vector3 pos = new Vector3();
        public Vector3 velocity = new Vector3();
        public Vector3 accel = new Vector3();
        public float mass = 1;

        public Color col = new Color();

        public float G = 1.0f;

        public void ApplyGravity(float scalar = 0.1f)
        {
            // (scalar * mass) / mass
            this.accel += (new Vector3(0, -scalar * mass, 0) / mass); 
        }

        public void ApplyForce(Vector3 force)
        {
            // f = ma
            // a = f/m
            this.accel += (force / mass); // force accumulation
        }

        public Vector3 CalculateAttraction(Movable target)
        {
            var force = this.pos - target.pos;
            var distance = force.GetMagnitude();

            // prevents gravitation if its beyond or is 25
            // prevents FURTHER gravitation if its less than or equal to 5
            distance = Utils.Constrain(distance, 5, 25);

            force.Normalize();

            var strength = (this.G * this.mass * target.mass) / (distance * distance);
            force *= strength;
            return force;
        }

        public abstract void Render(OpenGL gl);
    }
}
