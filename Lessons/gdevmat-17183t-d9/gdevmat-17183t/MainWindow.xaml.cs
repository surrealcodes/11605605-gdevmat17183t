﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            for (int x = 0; x < 10; x++)
            {
                myCubes.Add(new Cube()
                {
                    mass = (float)RandomNumberGenerator.GenerateDouble(5, 10),
                    pos = new Vector3(
                        (float)RandomNumberGenerator.GenerateDouble(-5.0, 5.0),
                        (float)RandomNumberGenerator.GenerateDouble(-5.0, 5.0),
                        0
                        ),
                    col = new Models.Color(
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0),
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0),
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0),
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0)
                        )
                });
            }

            for (int x = 0; x < 10; x++)
            {
                myCircles.Add(new Circle()
                {
                    mass = (float)RandomNumberGenerator.GenerateDouble(5, 10),
                    pos = new Vector3(
                        (float)RandomNumberGenerator.GenerateDouble(-5.0, 5.0),
                        (float)RandomNumberGenerator.GenerateDouble(-5.0, 5.0),
                        0
                        ),
                    col = new Models.Color(
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0),
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0),
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0),
                        (float)RandomNumberGenerator.GenerateDouble(0.1, 1.0)
                        ),
                    radius = (float)RandomNumberGenerator.GenerateDouble(1, 3)
                });
            }
        }

        // declarations
        // float rotation = 0.0f;
        
        private float time = 0.0f;

        private Vector3 mousePos = new Vector3();

        private Vector3 wind = new Vector3(0.05f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.5f, 0);
        private Vector3 helium = new Vector3(0, 0.5f, 0);

        public List<Cube> myCubes = new List<Cube>();
        public List<Circle> myCircles = new List<Circle>();
        public Cube emptyCube = new Cube()
        {
            mass = 5,
            pos = new Vector3(0, 0, 0)
        };

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT D7";
            OpenGL gl = args.OpenGL;

            // clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            mousePos.Normalize();
            // draw
            //gl.Rotate(rotation, 0.0f, 0.0f, 0.0f); // rotation value, bool, bool, bool
            //Teapot tp = new Teapot();
            //tp.Draw(gl, 14, 3, OpenGL.GL_FILL);
            //rotation += 30.0f;

            // time value
            // time += 0.1f
            
            foreach (var c in myCubes)
            {
                foreach (var c2 in myCircles)
                {
                    c.Render(gl);
                    c2.Render(gl);

                    if (c.pos.y <= 30 && c2.pos.y <= 30 && c.pos.y >= -30 && c2.pos.y >= -30 &&
                        c.pos.x <= 100 && c2.pos.x <= 100 && c.pos.x >= -100 && c2.pos.x >= -100)
                    {
                        c.ApplyForce(c2.CalculateAttraction(c) + emptyCube.CalculateAttraction(c));
                        c2.ApplyForce(c.CalculateAttraction(c2) + emptyCube.CalculateAttraction(c2));
                    }
                    else
                    {
                        c.velocity *= -1;
                        c2.velocity *= -1;
                    }
                }
            }
        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f};

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);


            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        #region MOUSE MOVEMENT
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePos.x = (float)position.X - (float)Width / 2.0f;
            mousePos.y = -((float)position.Y - (float)Height / 2.0f);
        }
        #endregion
    }
}
