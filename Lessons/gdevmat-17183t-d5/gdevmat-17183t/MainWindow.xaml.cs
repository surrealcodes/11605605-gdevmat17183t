﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // declarations
        // float rotation = 0.0f;
        
        private float time = 0.0f;
        public Cube myCube = new Cube(new Vector3(5.0f, 2.0f, 1.0f));

        public Circle myCircle = new Circle()
        {
            pos = new Vector3(0.0f, 0.0f, 1.0f),
            velocity = new Vector3(1.0f, 0.0f, 0.0f),
            accel = new Vector3(0.1f, 0.0f, 0.0f)
        };

        Vector3 val = new Vector3(1.0f, 2.0f, 0.0f);
        private Vector3 mousePos = new Vector3();

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT D5";
            OpenGL gl = args.OpenGL;

            // clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            // draw
            //gl.Rotate(rotation, 0.0f, 0.0f, 0.0f); // rotation value, bool, bool, bool
            //Teapot tp = new Teapot();
            //tp.Draw(gl, 14, 3, OpenGL.GL_FILL);
            //rotation += 30.0f;

            // time value
            time += 0.1f;
            if (time >= 360.0f)
            {        
                time = 0;
            }

            // y = mx + b;

            if (myCircle.pos.x > 35.0f)
            {
                myCircle.velocity *= -1;
            }

            myCube.pos += val;

            if (myCube.pos.x > 20 || myCube.pos.x < -20) val.x *= -1.0f;
            if (myCube.pos.y > 20 || myCube.pos.y < -20) val.y *= -1.0f;

            val /= 3.0f;

            mousePos.Normalize();

            // myCube.Render(gl);
            gl.LineWidth(10);
            gl.Color(1.0f, 0.0f, 0.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 0);
            gl.Vertex(mousePos.x * 8, mousePos.y * 8);
            gl.End();

            gl.LineWidth(5);
            gl.Color(1.0f, 1.0f, 1.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 0);
            gl.Vertex(myCube.pos.x, myCube.pos.y);
            gl.End();

            val.Normalize();
            

            myCube.Render(gl);

            myCircle.Render(gl);

            gl.DrawText(0, 0, 1.0f, 1.0f, 1.0f, "Arial", 12.0f, "X: " + mousePos.x.ToString() + "\tY: " + mousePos.y.ToString());
        }

        #region OPENGL INITIALIZATION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f};

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);


            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePos.x = (float)position.X - (float)Width / 2.0f;
            mousePos.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
