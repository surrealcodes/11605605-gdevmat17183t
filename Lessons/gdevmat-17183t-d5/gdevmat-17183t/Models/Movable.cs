﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public abstract class Movable
    {
        public Vector3 pos = new Vector3();
        public Vector3 velocity = new Vector3();
        public Vector3 accel = new Vector3();

        public Color col = new Color();

        public abstract void Render(OpenGL gl);
    }
}
